from Player import Player
from PlayerStrategy import PlayerStrategy
from Game import Game
import random

# Create the two players
player1 = Player('X')
player2 = Player('O')

player1_strategy = PlayerStrategy()
player2_strategy = PlayerStrategy()

# The game counter
id = 1

while True:
    # Create a new game
    game = Game(id, player1, player2)

    while True:
        ## PLAYER 1
        # Search best possible best move
        move = player1_strategy.findBestMove(game)
        if(move == None):
            # Get possibles moves
            moves = game.getPossiblesMoves()
            # Get random move
            move = random.choice(moves)
        # Lear at each move
        player1_strategy.learn(game, move)
        # Make the move
        game.board.move(game.player1, move)

        #game.board.print()

        if(game.hasWinner() == True):
            # Set the score
            player1_strategy.setScore(game, 10)
            print('Player 1 Win !')
            break

        if game.isGameOver() == True:
            # Set the score
            player1_strategy.setScore(game, 5)
            print('Tie !')
            break
        
        ## PLAYER 2
        # Get possibles moves
        moves = game.getPossiblesMoves()
        # Get random move
        move = random.choice(moves)
        # Make the move
        game.board.move(game.player2, move)
        # Lear at each move
        player2_strategy.learn(game, move)

        #game.board.print()

        if(game.hasWinner() == True):
            # Set the score
            player2_strategy.setScore(game, 10)
            player1_strategy.setScore(game, -5)
            print('==> Player 2 Win ! <==')
            break

        if game.isGameOver() == True:
            # Set the score
            player2_strategy.setScore(game, 5)
            print('Tie !')
            break
    # Increment the game counter
    id += 1