class PlayerStrategy:
    # Constructor
    def __init__(self):
        self.__memory = []
    
    # Learn at each move
    def learn(self, game, move):
        d = {
            'id': game.id,
            'hash': game.board.getGridHash(),
            'move': move,
            'score': 0
        }
        self.__memory.append(d)

    # Set the final score
    def setScore(self, game, score):
        for m in self.__memory:
            if m['id'] == game.id:
                m['score'] = score
        # print(self.__memory)

    # Find best possible moves
    def findBestPossibleMoves(self, game):
        hash = game.board.getGridHash()
        moves = {}
        for m in self.__memory:
            if m['hash'] == hash:
                if m['move'] in moves:
                    moves[m['move']] += m['score']
                else:
                    moves[m['move']] = m['score']
        # print(moves)
        return moves

    # Find best move
    def findBestMove(self, game):
        moves = self.findBestPossibleMoves(game)
        bestScore = 0
        bestMove = None
        for i, (move, score) in enumerate(moves.items()):
            if(score > bestScore):
                bestScore = score
                bestMove = move
        # print(bestMove)
        return bestMove