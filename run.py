from Player import Player
from Game import Game

player1 = Player('X')
player2 = Player('O')

game = Game(1, player1, player2)
game.autoplay()