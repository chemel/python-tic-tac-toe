import hashlib

class Board:
    def __init__(self):
        self.__board = [
            None, None, None,
            None, None, None,
            None, None, None
        ]

    def getBoard(self):
        return self.__board

    def move(self, player, index):
        if self.__board[index] == None:
            self.__board[index] = player.getSymbol()
        else:
            raise Exception('Position ' + index + ' already played')

    def print(self):
        print('------')
        print("|{0} {1} {2}|".format(self.__board[0] or ' ', self.__board[1] or ' ', self.__board[2] or ' '))
        print("|{0} {1} {2}|".format(self.__board[3] or ' ', self.__board[4] or ' ', self.__board[5] or ' '))
        print("|{0} {1} {2}|".format(self.__board[6] or ' ', self.__board[7] or ' ', self.__board[8] or ' '))
        print('------')

    def getGridHash(self):
        arr = []
        for v in self.__board:
            arr.append(v or '')
        arr = ','.join(arr)
        arr = arr.encode('utf-8')
        hash = hashlib.sha1(arr)
        return hash.hexdigest()