from Board import Board
import random

class Game:
    player1 = None
    player2 = None
    board = None
    turn = 1

    def __init__(self, id, player1, player2):
        self.id = id
        self.player1 = player1
        self.player2 = player2
        self.board = Board()

    def getPossiblesMoves(self):
        moves = []
        for i, v in enumerate(self.board.getBoard()):
            if v == None:
                moves.append(i)
        return moves

    def isGameOver(self):
        for i, v in enumerate(self.board.getBoard()):
            if v == None:
                return False
        return True

    def hasWinner(self):
        board = self.board.getBoard()
        # Check horizontal
        if board[0] != None and board[0] == board[1] and board[1] == board[2]:
            return True
        if board[3] != None and board[3] == board[4] and board[4] == board[5]:
            return True
        if board[6] != None and board[6] == board[7] and board[7] == board[8]:
            return True
        # Check vertical
        if board[0] != None and board[0] == board[3] and board[3] == board[6]:
            return True
        if board[1] != None and board[1] == board[4] and board[4] == board[7]:
            return True
        if board[2] != None and board[2] == board[5] and board[5] == board[8]:
            return True
        # Check diagobales
        if board[0] != None and board[0] == board[4] and board[4] == board[8]:
            return True
        if board[2] != None and board[2] == board[4] and board[4] == board[6]:
            return True
        return False

    def autoplay(self):
        while True:
            moves = self.getPossiblesMoves()
            move = random.choice(moves)
            self.board.move(self.player1, move)
            self.board.print()
            if(self.hasWinner() == True):
                print('Player 1 Win !')
                break
            if self.isGameOver() == True:
                print('Tie !')
                break
            
            moves = self.getPossiblesMoves()
            move = random.choice(moves)
            self.board.move(self.player2, move)
            self.board.print()
            if(self.hasWinner() == True):
                print('Player 2 Win !')
                break
            if self.isGameOver() == True:
                print('Tie !')
                break