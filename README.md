# Python Tic-tac-toe

Python Tic-tac-toe with reinforcement learning

## Run a single game

`python3 run.py`

## Run a game with reinforcement learning

`python3 learn.py`
